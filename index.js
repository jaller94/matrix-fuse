'use strict';
const path = require('path');
const Fuse = require('fuse-native');
const stat = function (st) {
    return {
        mtime: st.mtime || new Date(),
        atime: st.atime || new Date(),
        ctime: st.ctime || new Date(),
        size: st.size !== undefined ? st.size : 0,
        mode: st.mode === 'dir' ? 16877 : (st.mode === 'file' ? 33188 : (st.mode === 'link' ? 41453 : st.mode)),
        uid: st.uid !== undefined ? st.uid : process.getuid(),
        gid: st.gid !== undefined ? st.gid : process.getgid()
    }
}

const mnt = '/home/jaller94/mount-matrix/mnt/';

const { startClient } = require('./matrix');

const rooms = {};

const main = async () => {
  const client = await startClient();

  for (const roomId of await client.getJoinedRooms()) {
    rooms[roomId] = {};
    rooms[roomId].state = await client.getRoomState(roomId);
    console.log('get_state', roomId, rooms[roomId].state);
  }

  const ops = {
    readdir: async (path, cb) => {
      if (path === '/') return cb(null, ['rooms']);
      if (path === '/rooms') return cb(null, Object.keys(rooms));
      if (path.startsWith('/rooms/')) return cb(null, ['state.json']);
      return cb(Fuse.ENOENT);
    },
    getattr: function (path, cb) {
      if (path === '/') return cb(null, stat({ mode: 'dir', size: 4096 }));
      if (path === '/rooms') return cb(null, stat({ mode: 'dir', size: 4096 }));
      if (/^\/rooms\/.+?\/state\.json$/.test(path)) return cb(null, stat({ mode: 'file', size: 11 }));
      if (path.startsWith('/rooms/')) return cb(null, stat({ mode: 'dir', size: 4096 }));
      return cb(Fuse.ENOENT);
    },
    open: function (path, flags, cb) {
      return cb(0, 42);
    },
    release: function (path, fd, cb) {
      return cb(0);
    },
    read: function (path, fd, buf, len, pos, cb) {
      const match = path.match(/^\/rooms\/(.+?)\/state\.json$/);
      if (match && match[1] && rooms[match[1]]) {
        const str = (rooms[match[1]].state || '').slice(pos, pos + len);
        if (!str) return cb(0);
        buf.write(str);
        return cb(str.length);
      }
      return cb(0);
    }
  }

  const fuse = new Fuse(mnt, ops, { debug: true, force: true, mkdir: true })
  fuse.mount();
}

main().catch(console.error);
 
// fuse.mount(function (err) {
//   fs.readFile(path.join(mnt, 'test'), function (err, buf) {
//     console.log(err, buf);
//   })
// });
