'use strict';
const fsPromises = require('fs').promises;
const path = require('path');
const util = require('util');
// Olm has to be loaded before matrix-js-sdk
//global.Olm = require('olm');
const {
    LogLevel,
    LogService,
    MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,
} = require('matrix-bot-sdk');

LogService.setLevel(LogLevel.ERROR);

// We'll want to make sure the bot doesn't have to do an initial sync every
// time it restarts, so we need to prepare a storage provider. Here we use
// a simple JSON database.
const storage = new SimpleFsStorageProvider('webstorage/storage.json');

const {
    baseUrl,
    myUserId,
    myPassword,
    modules,
    privateRoom,
} = require('./config/matrix.js');

async function loadAccessToken() {
    return await fsPromises.readFile('webstorage/accesstoken.txt', 'utf8');
}

async function loginAndSaveToken() {
    const { MatrixAuth } = require('matrix-bot-sdk');
    const auth = new MatrixAuth(baseUrl);
    const result = await auth.passwordLogin(myUserId, myPassword);
    await fsPromises.writeFile('webstorage/accesstoken.txt', result.accessToken, 'utf8');
    return result.accessToken;
}

// == Plugins ==
async function startBot(accessToken) {
    //await login(localStorage, baseUrl, myUserId, myPassword);
    //console.log(localStorage.getItem('accessToken'));
    const client = new MatrixClient(
        baseUrl,
        accessToken,
        storage
    );
    AutojoinRoomsMixin.setupOnClient(client);

    // Listen for low-level MatrixEvents
    // let eventLogFile;
    // client.on('event', function(event) {
    //     console.log('event: ', event['content']['msgtype']);
    //     if (eventLogFile) {
    //         const data = util.inspect(event, {
    //             depth: 5,
    //             maxArrayLength: null,
    //         });
    //         fs.write(eventLogFile, data + '\n', null, 'utf8', (err) => {
    //             if (err) {
    //                 console.error('Error writing to event.log!');
    //                 console.error(err);
    //             }
    //         });
    //     }
    // });

    client.on("room.message", async (roomId, event) => {
        // Don't handle events that don't have contents (they were probably redacted)
        if (!event["content"]) return;

        // Don't handle non-text events
        //if (event["content"]["msgtype"] !== "m.text") return;

        // We never send `m.text` messages so this isn't required, however this is
        // how you would filter out events sent by the bot itself.
        if (event["sender"] === await client.getUserId()) return;

        // Make sure that the event looks like a command we're expecting
        // const body = event["content"]["body"];
        //if (!body || !body.startsWith("!hello")) return;

        // If we've reached this point, we can safely execute the command. We'll
        // send a reply to the user's command saying "Hello World!".
        // const replyBody = "Hello World!"; // we don't have any special styling to do.
        // const reply = RichReply.createFor(roomId, event, replyBody, replyBody);
        // reply["msgtype"] = "m.notice";
        // client.sendText(roomId, reply);
    });

    await client.start();
    console.log('Client started');
    console.log(await client.getJoinedRooms());
    return client;
}

async function startClient() {
    let accessToken;
    try {
        accessToken = await loadAccessToken();
    } catch {
        accessToken = await loginAndSaveToken();
    }
    return startBot(accessToken).catch((err) => {
        console.error('An error occured starting the bot!');
        console.error(err);
        process.exit(2);
    });
}

module.exports = {
    startClient,
};
